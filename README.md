# Cronos Rocket Organizational Repository

Welcome to the organizational repository for Cronos Rocket! This repository is used by the team to track organizational, communication, and non-repository-specific work.

## About the Cronos Rocket Project

Cronos Rocket is a hybrid-fueled sounding rocket designed and manufactured in an open and participatory manner by a team of university students at **Hackerspace.gr**. The project is engineered by **White Noise** in close collaboration with and under the auspices of **Libre Space Foundation**.

The primary aim of Project Cronos is to become a test platform for exploring future possibilities of building more advanced rockets, capable of reaching higher altitudes, designed in a collaborative and participatory way.

If you want to learn more about the Cronos Rocket project, please check out the source-code repositories.

## Contributing

Please note that this repository is used only for organizational, communication, and non-repository-specific work. For code-related contributions, please refer to the appropriate repository.

1. Navigate to the `Issues` section to view and contribute to organizational tasks and communications.
2. Create a comment to report a problem or suggest an improvement to the project.

